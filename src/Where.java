import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;

public class Where extends JPanel
{
	JButton button1 = new JButton("現在地から検索");
	JLabel text1 = new JLabel("傘ステーション選択");
	MainFrame mf;
	String str;
	public Where(MainFrame mf, String s)
	{
		this.mf = mf;
		str = s;
		this.setSize(400,400);
		text1.setVerticalAlignment(JLabel.TOP);
		text1.setHorizontalAlignment(JLabel.CENTER);
		text1.setFont(new Font("HGゴシック", Font.BOLD, 20));
		
		button1.setPreferredSize(new Dimension(300,50));
		button1.addActionListener(new ActionListener(){
			public void actionPerformed(ActionEvent e) {
				pc(mf.PanelNames[2]);
		}
	});
		
		JLabel label = new JLabel("駅名から検索");

		JTextField text2 = new JTextField(10);
		
		add(text1,BorderLayout.NORTH);
		add(button1,BorderLayout.CENTER);
		add(label,BorderLayout.SOUTH);
		add(text2);
		
		
	}
	public void pc(String str) {
		mf.PanelChange(str);
	}
}
