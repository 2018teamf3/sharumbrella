import java.io.FileInputStream;

import com.google.auth.oauth2.GoogleCredentials;
import com.google.firebase.FirebaseApp;
import com.google.firebase.FirebaseOptions;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

public class CountTime
{
  static String JSON_FILE_NAME = "C:\\Users\\ctctime24\\Downloads\\test-67cc6-60b70b74957b.json"; // 認証情報の JSONファイル名のパスを指定。
  static String URL = "https://test-67cc6.firebaseio.com/"; // Firebase上のリアルタイムデータベースのURLを指定。

  static String longitude;
  static String latitude;
  static String station;
  static String start_time;
  static String finish_time;


  public static void main(String[] args) throws Exception
  {
    System.out.println("mainメソッドの処理が開始されました。");

    // 初期化。

    FileInputStream serviceAccount = new FileInputStream(JSON_FILE_NAME);

    FirebaseOptions options = new FirebaseOptions.Builder()
        .setCredentials(GoogleCredentials.fromStream(serviceAccount))
        .setDatabaseUrl(URL)
        .build();

    FirebaseApp.initializeApp(options);

    // Firebase へのデータ送受信を行うサンプル

      DatabaseReference reference = FirebaseDatabase.getInstance().getReference();

      ValueEventListener listener = new ValueEventListener()
      {
        @Override
        public void onDataChange(DataSnapshot snapshot)
        {
          String key   = snapshot.getKey();
          Object value = snapshot.getValue();
          //System.out.println("データを受信しました。key=" + key + ", value=" + value);
          if (key=="key1") {
          	longitude=value.toString();
              System.out.println(longitude);
          }else {
          	if (key=="key2") {
          		latitude=value.toString();
          		System.out.println(latitude);			;
          	}else {
          		if(key=="key3") {
          			station=value.toString();
          			System.out.println(station);
          		}else {
          			if(key=="key4") {
              			start_time=value.toString();
              			System.out.println(start_time);
          			}else {
              			finish_time=value.toString();
              			System.out.println(finish_time);
          			}
          		}
          	}
          }
          int s_time = Integer.parseInt(start_time);
          int f_time = Integer.parseInt(finish_time);
          int diff_time=f_time - s_time;

          String spending_time=String.valueOf(diff_time);

          System.out.println("利用時間は"+spending_time+"秒");

        }

        @Override
        public void onCancelled(DatabaseError error)
        {
          System.out.println("キャンセルされました。");
        }
      };

      reference.child("key1").addValueEventListener(listener);
      reference.child("key2").addValueEventListener(listener);
      reference.child("key3").addValueEventListener(listener);
      reference.child("key4").addValueEventListener(listener);
      reference.child("key5").addValueEventListener(listener);


    // 上記のデータの送受信が終わるまで、しばらく時間がかかりますので、10秒間待機します。

    try
    {
      Thread.sleep(10000);
    }
    catch (Exception e)
    {
      e.printStackTrace();
    }

    System.out.println("mainメソッドの処理が終了しました。");
  }

}
