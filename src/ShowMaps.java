import java.awt.BorderLayout;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.Font;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.URL;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;

import com.google.auth.oauth2.GoogleCredentials;
import com.google.firebase.FirebaseApp;
import com.google.firebase.FirebaseOptions;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

public class ShowMaps
{
  static String JSON_FILE_NAME = "C:\\Users\\ctctime24\\Downloads\\test-67cc6-60b70b74957b.json"; // 認証情報の JSONファイル名のパスを指定。
  static String URL = "https://test-67cc6.firebaseio.com/"; // Firebase上のリアルタイムデータベースのURLを指定。
  static String latitude;
  static String longitude;
  static String station;
  static String start_time;
  static String finish_time;

  public static void main(String[] args) throws Exception
  {
	JFrame test = new JFrame("Google Maps");
	test.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
	test.setSize(700,700);
	test.setLocationRelativeTo(null);
    System.out.println("mainメソッドの処理が開始されました。");

    // 初期化。

    FileInputStream serviceAccount = new FileInputStream(JSON_FILE_NAME);

    FirebaseOptions options = new FirebaseOptions.Builder()
        .setCredentials(GoogleCredentials.fromStream(serviceAccount))
        .setDatabaseUrl(URL)
        .build();

    FirebaseApp.initializeApp(options);

    // Firebase へのデータ送受信を行うサンプル

//    	sampleReceivingDataFromFirebase1(); // Firebase からデータを受信するサンプル1
    DatabaseReference reference = FirebaseDatabase.getInstance().getReference();

    ValueEventListener listener = new ValueEventListener()
    {
      @Override
      public void onDataChange(DataSnapshot snapshot)
      {
        String key   = snapshot.getKey();
        Object value = snapshot.getValue();

        if (key=="key1") {
        	longitude=value.toString();
            System.out.println(longitude);
        }else {
        	if (key=="key2") {
        		latitude=value.toString();
        		System.out.println(latitude);			;
        	}else {
        		if(key=="key3") {
        			station=value.toString();
        			System.out.println(station);
        		}else {
        			if(key=="key4") {
            			start_time=value.toString();
            			System.out.println(start_time);
        			}else {
            			finish_time=value.toString();
            			System.out.println(finish_time);
        			}
        		}
        	}
        }
      }

      @Override
      public void onCancelled(DatabaseError error)
      {
        System.out.println("キャンセルされました。");
      }
    };

    reference.child("key1").addValueEventListener(listener);
    reference.child("key2").addValueEventListener(listener);
    reference.child("key3").addValueEventListener(listener);

    // 上記のデータの送受信が終わるまで、しばらく時間がかかりますので、10秒間待機します。

    try
    {
      Thread.sleep(4000);
    }
    catch (Exception e)
    {
      e.printStackTrace();
    }

    System.out.println("mainメソッドの処理が終了しました。");


  try {
		String imageUrl = "https://maps.googleapis.com/maps/api/staticmap?center="+ latitude+ ","+ longitude+ "&zoom=15&size=612x612&scale=2&maptype=roadmap";
		String destinationFile = "image.jpg";


		// read the map image from Google
		// then save it to a local file: image.jpg


		URL url = new URL(imageUrl);
		InputStream is = url.openStream();
		OutputStream os = new FileOutputStream(destinationFile);
		byte[] b = new byte[2048];


		int length;
		while ((length = is.read(b)) != -1) {
			os.write(b, 0, length);
		}

		is.close();
		os.close();
	} catch (IOException e) {
		e.printStackTrace();
		System.exit(1);
	}

	// create a GUI component that loads the image: image.jpg


	ImageIcon imageIcon = new ImageIcon((new ImageIcon("image.jpg")).getImage().getScaledInstance(630, 600,java.awt.Image.SCALE_SMOOTH));
	test.add(new JLabel(imageIcon));

	// ラベルのインスタンスを生成
	JLabel label = new JLabel("Google Map");
	label.setFont(new Font("Century", Font.ITALIC, 24));
	// ボタンのインスタンスを生成
	JButton button = new JButton(station+"付近");
	button.setFont(new Font("ＭＳゴシック", Font.BOLD, 20));
	button.setMaximumSize(new Dimension(200, 50));
	Container contentPane = test.getContentPane();
	// ラベルをContentPaneに配置
	contentPane.add(label, BorderLayout.NORTH);
	// ボタンをContentPaneに配置
	contentPane.add(button, BorderLayout.SOUTH);

	test.setVisible(true);
	}
<<<<<<< HEAD

=======
>>>>>>> sanchan
}
